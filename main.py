from pymongo import MongoClient

# conn_str = "mongodb+srv://admin:Og3OlRtElPIJ70mm@smartstop.rwxpfwl.mongodb.net/SmartStops"
# Definimos las constantes
stops = [
    "UTMA",
    "Terminal Villas, Sector Puertecito",
    "Calle Puerto De Cozumel"
]
peoplePerAutobus = 60


def getdatabase():
    conn_str = "localhost"
    client = MongoClient(conn_str)
    return client["SmartStops"]


def isValidTime(value):
    separator = value.split("-")
    permithours = ["6", "7", "8", "9", "10", "11", "12", "13",
                   "14", "15", "16", "17", "18", "19", "20", "21"]
    if len(separator) != 2:
        return False
    else:
        condition = separator[1] == str(int(separator[0]) + 1) and separator[0] in permithours and separator[
            1] in permithours
        if condition:
            return True
        else:
            return False


def isValidStop(stop):
    if 0 < int(stop) <= 3:
        postion = int(stop) - 1
        if stops[postion]:
            return True
        else:
            return False
    else:
        return False


try:
    # Jalamos nuestra DB
    db = getdatabase()
    # Declaramos la coleccion con la cual vamos a trabajar
    collection = db['answers']
    # Inputs
    status = False
    while not status:
        route = int(input("Ingrese el numero de ruta, 8, 37 o 43: "))
        if route != 37 and route != 8 and route != 43:
            print("ruta invalida")
        else:
            status = True
    status = False
    while not status:
        time = input("Ingrese el tiempo en el cual toma la ruta "
                     "(considere este formato: Tiempo en 12hrs y HH-HH, ejem: 12-13): ")
        response = isValidTime(time)
        if response:
            status = True
        else:
            print("Hora invalida")
    status = False
    while not status:
        stop = input(" Ingrese la parada a la cual quiere ir: "
                     "Estas son las opciones:"
                     "\n1 para " + stops[0] +
                     "\n2 para " + stops[1] +
                     "\n3 para " + stops[2] + "\n")
        response = isValidStop(stop)
        if response:
            stop = stops[int(stop) - 1]
            status = True
        else:
            print("Parada invalida")
    # Imprimir respuestas recibidas
    print("Sus respuestas fueron:\n" +
          "Ruta:", route, "\n"
                          "Hora:", time, "\n"
                                         "Parada:", stop)

    # Jalamaos la informacion de nuestra coleccion y comenzamos a hacer calculos
    quantityForFirstStop = []
    quantityForSecondStop = []
    quantityForThirdStop = []
    register = collection.find({})
    for document in collection.find({}):
        if document["route"] == route:
            if document["time"] == time:
                if document["stop"]["name"] == stops[0]:
                    quantityForFirstStop.append(document)
                elif document["stop"]["name"] == stops[1]:
                    quantityForSecondStop.append(document)
                elif document["stop"]["name"] == stops[2]:
                    quantityForThirdStop.append(document)
    lowest = "no determined"
    medium = "no determined"
    highest = "no determined"
    if len(quantityForFirstStop) > len(quantityForSecondStop) and len(quantityForFirstStop) > len(quantityForThirdStop):
        highest = stops[0]
        if quantityForSecondStop > quantityForThirdStop:
            medium = stops[1]
            lowest = stops[2]
        else:
            medium = stops[2]
            lowest = stops[1]
    if len(quantityForSecondStop) > len(quantityForFirstStop) and len(quantityForSecondStop) > len(
            quantityForThirdStop):
        highest = stops[1]
        if quantityForFirstStop > quantityForThirdStop:
            medium = stops[0]
            lowest = stops[2]
        else:
            medium = stops[2]
            lowest = stops[0]
    if len(quantityForThirdStop) > len(quantityForSecondStop) and len(quantityForThirdStop) > len(quantityForFirstStop):
        highest = stops[2]
        if quantityForFirstStop > quantityForSecondStop:
            medium = stops[0]
            lowest = stops[1]
        else:
            medium = stops[1]
            lowest = stops[0]

    print("Se realizo el calculo y acorde los resultado se definio lo siguiente: \n"
          "La parada con menos personas es: ", lowest, "\n"
           "La parada con una cantidad media de personas es: ", medium, "\n"
            "La parada con mayor personas es: ", highest, "\n")
    if lowest == stops[0]:
        condition = len(quantityForFirstStop) > peoplePerAutobus
    if lowest == stops[1]:
        condition = len(quantityForSecondStop) > peoplePerAutobus
    if lowest == stops[2]:
        condition = len(quantityForThirdStop) > peoplePerAutobus

    if condition:
        print("Lo sentimos pero la con menos personas es .", lowest, "y ya tiene mas de 60 personas esperando"
                                                                     ,"se recomienda consultar en otra hora")
    elif lowest == stop and not condition:
        print("La parada que usted eligio es la mas apta para subir al camion.", lowest)
    else:
        print("Estamos consicientes de que usted queria subir en la parada:", stop, "pero deacuerdo a los resultados",
              "la mas apta para subir al camion es:", lowest)

    exit()

except Exception:
    print("Valio verga no esta viendo")
